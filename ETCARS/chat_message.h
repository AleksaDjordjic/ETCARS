#pragma once
#include "prerequisites.h"
#include "Debugger.h"
namespace Debugger
{
	extern void init(scs_log_t log, const char* logFileName);
	extern void log(LogTo log, LogLevel level, const char* message);
	extern void log_line(const char* message);
	extern void finish_log();
	extern const char* currentTime();
	extern const char* logLevelToString(LogLevel level);
#ifdef _WIN32
	extern scs_log_t game_log;
	extern  FILE *log_file;
	extern  time_t now;
	extern  tm localTime;
	extern  char timeBuffer[4096];
	extern  char* timeNBuffer;
	extern  std::string _logLine;
#endif
}

#ifndef CHAT_MESSAGE_HPP
#define CHAT_MESSAGE_HPP
class chat_message
	: public boost::enable_shared_from_this<chat_message>
{
public:
	enum { header_length = 8 };
	enum { max_body_length = 36000 };

	chat_message()
		: body_length_(0)
	{

	}
	~chat_message()
	{
		flexData = "";
	}

	

	std::size_t length() const
	{
		
		return header_length + body_length_;
	}

	void setMessage(std::string msg)
	{
		flexData = msg;
	}

	std::size_t body_length() const
	{
		
		return body_length_;
	}

	void body_length(std::size_t new_length)
	{
		
		body_length_ = new_length;
		
		if (static_cast<int>(body_length_) > static_cast<int>(alloc_size))
		{
			alloc_size = body_length_+30;
		}
	}

	bool decode_header()
	{
		char header[header_length + 1] = "";
		for (size_t i = 0; i < header_length; i++)
		{
			header[i] = flexData.at(i);
		}
		body_length_ = std::atoi(header);

		if (static_cast<int>(body_length_) > static_cast<int>(alloc_size))
		{
			alloc_size = body_length_+30;
			
		}
		flexData = flexData.substr(header_length);
		return true;
	}

	void encode_header()
	{
		std::string bak = flexData;
		flexData = "";
		std::string size = std::to_string(bak.size());
		flexData += size;
		size_t size_len = size.size();
		for (size_t i = 8 - size_len; i > 0; i--)
		{
			flexData += " ";
		}
		flexData += bak;
		bak = "";
		size = "";
	}
	std::string flexData = "";
private:
	std::size_t body_length_;

	std::size_t alloc_size = 36;
};

#endif // CHAT_MESSAGE_HPP