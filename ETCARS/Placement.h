#pragma once
class Placement
{
public:
	Placement();
	Placement(float x, float y, float z, float heading, float pitch, float roll);

	void SetX(float val);
	void SetY(float val);
	void SetZ(float val);
	void SetPitch(float val);
	void SetHeading(float val);
	void SetRoll(float val);

	float GetX();
	float GetY();
	float GetZ();
	float GetPitch();
	float GetHeading();
	float GetRoll();

private:
	float x;
	float y;
	float z;
	float heading;
	float pitch;
	float roll;
};