#pragma once
#include "prerequisites.h"

class TransportLocation
{
public:
	TransportLocation();
	TransportLocation(float startX, float startY, float startZ, std::string startLoc, float endX , float endY , float endZ , std::string endLoc , float fee , float distance,std::string startLocID, std::string endLocID );
	TransportLocation(Placement start, Placement end, std::string startLoc, std::string endLoc, float fee, float distance,std::string startLocID, std::string endLocID);
	void SetStartX(float val);
	void SetStartY(float val);
	void SetStartZ(float val);
	void SetStartLoc(std::string loc);

	void SetEndX(float val);
	void SetEndY(float val);
	void SetEndZ(float val);
	void SetEndLoc(std::string loc);

	void SetFee(float val);
	void SetDistanceTravelled(float val);
    void SetStartLocID(std::string locID);
    void SetEndLocID(std::string locID);


	float GetStartX();
	float GetStartY();
	float GetStartZ();
	std::string GetStartLoc();
	float GetEndX();
	float GetEndY();
	float GetEndZ();
	std::string GetEndLoc();
	float GetFee();
	float GetDistanceTravelled();
    std::string GetStartLocID();
    std::string GetEndLocID();
private:
	Placement startCoordinates;
	std::string startLocation;
	Placement endCoordinates;
	std::string endLocation;
	float fee;
	float distanceTravelled;
    std::string startLocID;
    std::string endLocID;
};
