#include "Telemetry.h"

Telemetry::Telemetry() {
	this->game = new Game();
	this->job = new Job();
	this->navigation = new Navigation();
	this->pluginVersion = new PluginVersion();
	this->truck = new Truck();
	this->trailer=new Trailer();
	this->lastTimestamp = static_cast<scs_timestamp_t>(-1);
}

#pragma region Static Telemetry Instance
Telemetry* Telemetry::telemetry()
{
	if (!Telemetry::_telemetry)
		Telemetry::_telemetry = new Telemetry();
	return Telemetry::_telemetry;
}
Telemetry* Telemetry::_telemetry = 0;
#pragma endregion

#pragma region Plugin Version
Telemetry::PluginVersion::PluginVersion()
{
	this->majorVersion = 0;
	this->minorVersion = 1;
	this->pluginVersionOnlyStr = "0.1";
}
#pragma endregion

#pragma region Game
Telemetry::Game::Game()
{
	this->gameID = "";
	this->gameName = "";
	this->gameVersionStr = "";
	this->localScale = 0.00f;
	this->dateTime = 0;
	this->paused = true;
	this->isDriving = false;
	this->nextRestStop = 0;
	this->minorVersion = 0;
	this->majorVersion = 0;
	this->isMultiplayer = false;
	this->osEnvironment = "";
	this->architecture = "";
}
#pragma endregion

#pragma region Truck
Telemetry::Truck::Truck()
{

	this->damages = new Damages();
	this->fuel = new Fuel();
	this->warnings = new Warnings();
	this->lights = new Lights();
	this->speed = 0.00f;
	this->cruiseControlSpeed = 0.00f;
	this->parkingBrake = false;
	this->motorBrake = false;
	this->engineEnabled = false;
	this->electricsEnabled = false;
	this->odometer = 0.00f;
	this->make = "";
	this->model = "";
	this->makeID = "";
	this->modelID = "";
	this->shifterType = "";
	this->worldPlacement = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->trailerWorldPlacement = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->gear = 0;
	this->gearDisplayed = 0;
	this->engineRPM = 0.00f;
	this->retarderBrakeLevel = 0;
	this->brakeTemperature = 0.00f;
	this->fuelRange = 0.00f;
	this->wipersOn = false;

	this->retarderStepCount = 0;
	this->differentialRatio = 0.00f;
	this->forwardGearCount = 0;
	this->reverseGearCount = 0;
	this->maxEngineRPM = 0.00f;
	this->trailerConnected = false;
	this->hasTruck = false;
	this->linearVelocity = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->angularVelocity = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->linearAcceleration = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->angularAcceleration = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->hookPosition = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->cabinOffset = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->headPosition = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->cabinAngularVelocity = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->cabinAngularAcceleration = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->waterTemperature = 0.00f;
	this->batteryVoltage = 0.00f;
	this->oilTemperature = 0.00f;
	this->wheelCount = 0;
	for(int a = 0; a < MAX_WHEEL_COUNT; a++)
	{
		this->suspensionDeflections[a] = 0.00f;
		this->wheelOnGround[a] = false;
		this->wheelSubstance[a] = 0;
		this->wheelAngularVelocity[a] = 0.00f;
		this->wheelLift[a] = 0.00f;
		this->wheelLiftOffset[a] = 0.00f;
		this->wheelPosition[a] = Placement(0.00f,0.00f,0.00f,0.00f,0.00f,0.00f);
		this->wheelSteerable[a] = false;
		this->wheelSimulated[a] = false;
		this->wheelRadius[a] = 0.00f;
		this->wheelSteering[a] = 0.00f;
		this->wheelRotation[a] = 0.00f;
		this->wheelPowered[a] = false;
		this->wheelLiftable[a] = false;
	}
	this->inputSteering = 0.00f;
	this->inputThrottle = 0.00f;
	this->inputBrake = 0.00f;
	this->inputClutch = 0.00f;
	this->effectiveSteering = 0.00f;
	this->effectiveThrottle = 0.00f;
	this->effectiveBrake = 0.00f;
	this->effectiveClutch = 0.00f;
	this->hShifterSlot = 0;
	this->brakeAirPressure = 0.00f;
	this->adBlue = 0.00f;
	this->adBlueConsumptionAverage = 0.00f;
	this->dashboardBacklight = 0.00f;


}
#pragma endregion

#pragma region Warnings
Telemetry::Truck::Warnings::Warnings()
{
	this->airPressure = false;
	this->airPressureEmergency = false;
	this->batteryVoltage = false;
	this->fuelLow = false;
	this->oilPressure = false;
	this->waterTemperature = false;
	this->adBlue = false;
}
#pragma endregion

#pragma region Fuel
Telemetry::Truck::Fuel::Fuel()
{
	this->capacity = 0.00;
	this->warningLevel = 0.00;
	this->currentLitres = 0.00;
	this->consumptionAverage = 0.00;	
}
#pragma endregion

#pragma region Job
Telemetry::Job::Job()
{
	this->cargoID = "";
	this->cargo = "";
	this->mass = 0.00;
	this->destinationCityID = "";
	this->destinationCity = "";
	this->destinationCompanyID = "";
	this->destinationCompany = "";
	this->sourceCityID = "";
	this->sourceCity = "";
	this->sourceCompanyID = "";
	this->sourceCompany = "";
	this->income = 0;
	this->deliveryTime = 0;
	this->damage = 0.00;
	this->onJob = false;
	this->wasFinished = false;
	this->isLate = false;
	this->timeRemaining = 0;
}
#pragma endregion 

#pragma region Damages
Telemetry::Truck::Damages::Damages()
{
	this->engine = 0.00;
	this->transmission = 0.00;
	this->cabin = 0.00;
	this->chassis = 0.00;
	this->wheels = 0.00;
}
#pragma endregion

#pragma region Navigation
Telemetry::Navigation::Navigation()
{
	this->distance = 0.00;
	this->time = 0.00;
	this->speedLimit = 0.00;
	this->lowestDistance = 0.00;
	this->highestDistance = 0.00;
}
#pragma endregion

#pragma region Lights
Telemetry::Truck::Lights::Lights()
{
	this->beacon = false;
	this->frontAux = 0;
	this->highBeam = false;
	this->lowBeam = false;
	this->roofAux = 0;
	this->leftBlinker = new Blinker();
	this->rightBlinker = new Blinker();
	this->parking = false;
	this->brake = false;
	this->reverse = false;
}
#pragma endregion

#pragma region Blinkers
Telemetry::Truck::Lights::Blinker::Blinker()
{
	this->isOn = false;
	this->isEnabled = false;
}
#pragma endregion

#pragma region Trailer
Telemetry::Trailer::Trailer()
{
	this->wheelCount = 0;
	for(int a = 0; a < MAX_WHEEL_COUNT; a++)
	{
		this->wheelSuspensionDeflections[a] = 0.00f;
		this->wheelOnGround[a] = false;
		this->wheelSubstance[a] = 0;
		this->wheelAngularVelocity[a] = 0.00f;
		this->wheelPosition[a] = Placement(0.00f,0.00f,0.00f,0.00f,0.00f,0.00f);
		this->wheelSteerable[a] = false;
		this->wheelSimulated[a] = false;
		this->wheelRadius[a] = 0.00f;
		this->wheelPowered[a] = false;
		this->wheelSteering[a] = 0.00f;
		this->wheelRotation[a] = 0.00f;
	}
	this->linearVelocity = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->angularVelocity = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->linearAcceleration = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->angularAcceleration = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
	this->hookPosition = Placement(0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f);
}
#pragma endregion